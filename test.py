# coding=utf-8

import regex as re

class FileOpener:

    @staticmethod
    def read_test_file(path):
        result = {}
        with open(path, 'r') as file:
            for line in file:
                wordnet_id, wikipedia_entry, meaning_number = FileOpener._get_elements(line)
                result[wordnet_id] = (wikipedia_entry, meaning_number)
        return result

    @staticmethod
    def _get_elements(line):
        pattern = '^(\d*): (.*) (\d+);'
        match = re.search(pattern, line)
        if match:
            wordnet_id = match.group(1)
            wikipedia_entry = match.group(2)
            meaning_number = match.group(3)
            return wordnet_id, wikipedia_entry, meaning_number

