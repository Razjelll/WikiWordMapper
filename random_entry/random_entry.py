from loaders.wiktionary import WiktionaryOpener
from loaders.wordnet import WordnetOpener
from values import PartOfSpeech
import random

class EntryRandomizer:

    @staticmethod
    def start(wordnet1_path, wordnet2_path, wiktionary_path):
        NUMBERS = 100
        wiktionary = WiktionaryOpener.load_wiktionary(wiktionary_path)
        wordnet = WordnetOpener.load_wordnet(wordnet1_path, wordnet2_path)
        words = [word for word, units in wiktionary.items()]
        counter = 0
        words_result = []
        while counter < NUMBERS:
            found = False
            while not found:
                word = random.choice(words)
                units = wiktionary[word]
                if EntryRandomizer._check_pos(units, PartOfSpeech.NOUN) and word in wordnet:
                    print()
                    found = True
                    counter += 1
                    words_result.append(word)
        return words_result

    @staticmethod
    def _check_pos(units, pos):
        return pos in [unit.part_of_speech for unit in units]
