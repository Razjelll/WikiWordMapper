import morfeusz2
from values import PartOfSpeech
import abc
from nltk import WordNetLemmatizer, word_tokenize, pos_tag
from textblob import TextBlob

NOUN_TAGS = ['subst', 'depr']
ADJECTIVE_TAGS = ['adj', 'adja', 'adjp']
VERB_TAGS = ['fin', 'bedzie', 'aglt', 'praet', 'impt', 'imps', 'inf', 'pcon', 'pant', 'ger', 'pact', 'ppas']
ADVERBS_TAG = ['adv']


class Analyzer():

    WORD = 0
    POS = 1

    @abc.abstractmethod
    def analyse(self, sentence):
        pass

    @abc.abstractmethod
    def get_base_words(self, sentence):
        pass

class PolishAnalyzer(Analyzer):

    def __init__(self):
        self._analyzer = morfeusz2.Morfeusz()

    def analyse(self, sentence):
        result_list = []
        analyse_result = self._analyzer.analyse(sentence)
        for result in analyse_result:
            word = self._get_base_form_from_analyse_result(result)
            pos = self._get_pos_from_analyse_result(result)
            result_list.append((word, pos))
        return result_list

    def _get_base_form_from_analyse_result(self, analyse_result):
        return analyse_result[2][1].split(':')[0]

    def get_base_words(self, sentence):
        analyze_result = self._analyzer.analyse(sentence)
        return [self._get_base_form_from_analyse_result(result) for result in analyze_result]

    def _get_pos_from_analyse_result(self, analyse_result):
        text = analyse_result[2][2].split(':')[0]
        return self._get_part_of_speech_by_tag(text)

    def _get_part_of_speech_by_tag(self, tag):
        if tag in NOUN_TAGS:
            return PartOfSpeech.NOUN
        if tag in VERB_TAGS:
            return PartOfSpeech.VERB
        if tag in ADJECTIVE_TAGS:
            return PartOfSpeech.ADJECTIVE
        if tag in ADVERBS_TAG:
            return PartOfSpeech.ADVERB
        return None

# TODO: przetestować tę klasę
class EnglishAnalyzer(Analyzer):

    def analyse(self, sentence):
        tokens = word_tokenize(sentence)
        tagged_words = pos_tag(tokens)
        result = []
        for i, element in enumerate(tagged_words):
            word = element[0]
            word_blob = TextBlob(word)
            if len(word_blob.words) > 0:
                word = word_blob.words[0].singularize()
            tag = element[1]
            pos = self._get_pos(tag)
            result.append((word, pos))
        return result

    def _get_pos(self, tag):
        map = {'v':PartOfSpeech.VERB,
                'n': PartOfSpeech.NOUN,
                'j':PartOfSpeech.ADJECTIVE,
               'r': PartOfSpeech.ADVERB}
        tag = tag[0].lower()
        if tag in map:
            return map[tag]
        return None
