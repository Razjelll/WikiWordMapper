# coding=utf-8
from mappers.extra.inter_mapping import MappingLoader, MappingConnector
from mappers.mapper import Mapper, Saver
from mappers.weight_test import WeightTester
from random_entry.random_entry import EntryRandomizer
from test import FileOpener
from loaders.wiktionary import WiktionaryOpener
from loaders.wordnet import WordnetOpener
from values import Language
import pickle

MAIN_WORDNET_PATH = '/media/roman/SeagateExpansion/Projects/LUMapper/output/source_wordnet_2.json'
SECONDARY_WORDNET_PATH = '/media/roman/SeagateExpansion/Projects/LUMapper/output/target_wordnet_2.json'
POLISH_WIKTIONARY_PATH = '/media/roman/SeagateExpansion/Projects/Wiktionary/output/wiki_pl.json'
ENGLISH_WIKTIONARY_PATH = '/media/roman/SeagateExpansion/Projects/Wiktionary/output/wiki_en3.json'
TEST_FILE_PATH = '/media/roman/SeagateExpansion/Dokumenty/Praca/test.txt'
ENGLISH_TEST_FILE_PATH = '/media/roman/SeagateExpansion/Dokumenty/Praca/test_eng.txt'

POLISH_MAPPER_RESULT_PATH = 'output/pl_result.json'
ENGLISH_MAPPER_RESULT_PATH = 'output/en_result.json'

def run():
    # _open_wiktionary()
    # _open_wordnet()
    # _read_test_file()
    # _test()
    # _test2()
    # _test_eng()
    # _test_weights()
    # _start_mapper()
    # _start_random_entry()
    # _join_interlingual_mapping()
    _run_mapper()

def _open_wordnet():
    result = WordnetOpener.load_wordnet(MAIN_WORDNET_PATH, SECONDARY_WORDNET_PATH)
    print(result)

def _open_wiktionary():
    result = WiktionaryOpener.load_wiktionary(POLISH_WIKTIONARY_PATH)
    print(result)

def _read_test_file():
    test_entries = FileOpener.read_test_file(TEST_FILE_PATH)
    print()

def _test():
    mapper = Mapper(Language.PL)
    result = mapper.test(MAIN_WORDNET_PATH, SECONDARY_WORDNET_PATH, POLISH_WIKTIONARY_PATH, TEST_FILE_PATH)

def _test2():
    mapper = Mapper(Language.PL)
    result = mapper.test2(MAIN_WORDNET_PATH, SECONDARY_WORDNET_PATH, POLISH_WIKTIONARY_PATH, TEST_FILE_PATH)

def _test_eng():
    mapper = Mapper(Language.EN)
    result = mapper.test2(SECONDARY_WORDNET_PATH, MAIN_WORDNET_PATH, ENGLISH_WIKTIONARY_PATH, ENGLISH_TEST_FILE_PATH)

def _test_weights():
    WeightTester.test(MAIN_WORDNET_PATH, SECONDARY_WORDNET_PATH, POLISH_WIKTIONARY_PATH, TEST_FILE_PATH)

def _start_mapper():
    mapper = Mapper(Language.PL)
    map_result = mapper.run(MAIN_WORDNET_PATH, SECONDARY_WORDNET_PATH, POLISH_WIKTIONARY_PATH)

def _start_random_entry():
    words_list = EntryRandomizer.start(SECONDARY_WORDNET_PATH, MAIN_WORDNET_PATH ,ENGLISH_WIKTIONARY_PATH)
    with open('output/random_english_words.txt', 'w') as file:
        for word in words_list:
            file.write(word)
            file.write('\n')

def _join_interlingual_mapping():
    path ='/media/roman/SeagateExpansion/Projects/Wiktionary/output/pl_en_mapping.json'
    result = MappingLoader.load(path)
    wiktionary = WiktionaryOpener.load_wiktionary(ENGLISH_WIKTIONARY_PATH)
    MappingConnector.connect(wiktionary, result)

def _run_mapper():
    # mapper = Mapper(Language.PL)
    # result = mapper.run(MAIN_WORDNET_PATH, SECONDARY_WORDNET_PATH, POLISH_WIKTIONARY_PATH)
    # Saver.save(result, POLISH_MAPPER_RESULT_PATH)

    mapper = Mapper(Language.EN)
    result = mapper.run(SECONDARY_WORDNET_PATH, MAIN_WORDNET_PATH, ENGLISH_WIKTIONARY_PATH)
    Saver.save(result, ENGLISH_MAPPER_RESULT_PATH)




run()