
class RelationTypes:
    SYNONYM = 0
    ANTONYM = 1
    HYPERNYM = 2
    HYPONYM = 3
    MERONYM = 4
    HOLONYM = 5
    I_SYNONYM = 6

class PartOfSpeech:
    NOUN = 0
    VERB = 1
    ADJECTIVE = 2
    ADVERB = 3
    CONJUNCTION = 4
    # TODO: można dodac jeszcze pozostałe

class Language:
    PL = 0
    EN = 1

REGISTERS = {
    1: 'og',
    2: 'daw',
    3: 'książk',
    4: 'nienorm',
    5: 'posp',
    6: 'pot',
    7: 'reg',
    8: 'specj',
    9: 'środ',
    10: 'urz',
    11: 'wulg'
}

DOMAINS = {
    1: 'bhp',
    2: 'czy',
    3: 'wytw',
    4: 'cech',
    5: 'czc',
    6: 'umy',
    7: 'por',
    8: 'zdarz',
    9: 'czuj',
    10: 'jedz',
    11: 'grp',
    12: 'msc',
    13: 'cel',
    14: 'rz',
    15: 'os',
    16: 'zj',
    17: 'rsl',
    18: 'pos',
    19: 'prc',
    20: 'il',
    21: 'zw',
    22: 'ksz',
    23: 'st',
    24: 'sbst',
    25: 'czas',
    26: 'zwz',
    27: 'hig',
    28: 'zmn',
    29: 'cumy',
    30: 'cpor',
    31: 'wal',
    32: 'cjedz',
    33: 'dtk',
    34: 'cwyt',
    35: 'cczuj',
    36: 'ruch',
    37: 'pst',
    38: 'cpos',
    39: 'sp',
    40: 'cst',
    41: 'pog',
    42: 'jak',
    43: 'rel',
    44: 'odcz',
    45: 'grad',
    46: 'sys',
    47: 'adj',
    48: 'adv',
    49: 'mat',
    50: 'cdystr',
    51: 'caku',
    52: 'cper',
    53: 'cdel',
    54: '!nie ustawiony!',
    55: 'P',
    56: 'G',
    57: 'H',
    58: 'SI',
    59: 'O',
    60: 'Allgemein',
    61: 'Bewegung',
    62: 'Gefuehl',
    63: 'Geist',
    64: 'Gesellschaft',
    65: 'Koerper',
    66: 'Menge',
    67: 'natPhaenomen',
    68: 'Ort',
    69: 'Pertonym',
    70: 'Perzeption',
    71: 'privativ',
    72: 'Relation',
    73: 'Substanz',
    74: 'Verhalten',
    75: 'Zeit',
    76: 'Artefakt',
    77: 'Attribut',
    78: 'Besitz',
    79: 'Form',
    80: 'Geschehen',
    81: 'Gruppe',
    82: 'Kognition',
    83: 'Kommunikation',
    84: 'Mensch',
    85: 'Motiv',
    86: 'Nahrung',
    87: 'natGegenstand',
    88: 'Pflanze',
    89: 'Tier',
    90: 'Tops',
    91: 'Koerperfunktion',
    92: 'Konkurrenz',
    93: 'Kontakt',
    94: 'Lokation',
    95: 'Schoepfung',
    96: 'Veraenderung',
    97: 'Verbrauch'
}