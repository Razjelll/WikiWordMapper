import json

from loaders.wiktionary import WiktionaryOpener


class MappingMeaning:

    def __init__(self, word, position, pos, definition, entry):
        self.word = word
        self.position = position
        self.pos = pos
        self.definition = definition
        self.entry = entry

class MappingLoader:
    ''' Klasa ładująca rzutowanie pomiędzy dwoma wersjami językowymi Wikisłonika'''

    @staticmethod
    def load(path):
        result = []
        with open(path, 'r') as file:
            mapping_map = json.load(file)
            for key, mapping_meaning in mapping_map.items():
                word, position, pos, definition = MappingLoader._read_key(key)
                entry = WiktionaryOpener._load_entry(word, position, mapping_meaning)
                meaning = MappingMeaning(word, position, pos, definition, entry)
                result.append(meaning)
        return result

    @staticmethod
    def _read_key(key):
        parts = key.split(':')
        word = parts[0].rstrip()
        position = int(parts[1].rstrip())
        pos = int(parts[2].rstrip())
        definition = parts[3].rstrip()
        return word, position, pos, definition

class MappingConnector:

    @staticmethod
    def connect(wiktionary, mapping_result):
        # TODO: zrobić rzutowanie
        mappings_map = MappingConnector._create_mapping_result_map(mapping_result)
        for word, entries in wiktionary.items():
            for index, entry in enumerate(entries):
                key = MappingConnector._create_key_from_entry(entry, index)
                if key in mappings_map:
                    mapped_meaning = mappings_map[key]
                    # TODO: sprawdzić, co jest przyczyną NoneType
                    if entry.relations is not None:
                        entry.relations.update(mapped_meaning.relations)
                    if entry.translations is not None:
                        entry.translations.extend(mapped_meaning.translations)
                    if entry.examples is not None:
                        examples = MappingConnector._get_examples(mapped_meaning.examples)
                        entry.examples.extend(examples)
                    if entry.registers is not None:
                        entry.registers.extend(mapped_meaning.registers)
        return wiktionary

    @staticmethod
    def _get_examples(examples):
        result = []
        for example in examples:
            if '→' in example:
                end_index = example.index('→')
                filtered_example = example[:end_index].replace("'",'')
                result.append(filtered_example)
            else:
                result.append(example)
                print(example)
        return result

    @staticmethod
    def _create_mapping_result_map(mapping_result):
        result = {}
        for mapping in mapping_result:
            key = MappingConnector._create_key_from_mapping(mapping)
            value = mapping.entry
            result[key] = value
        return result

    @staticmethod
    def _create_key_from_mapping(mapping):
        word = mapping.word
        position = mapping.position

        return MappingConnector._create_key(word, position)

    @staticmethod
    def _create_key(word, position):
        return '{}:{}'.format(word, position)

    @staticmethod
    def _create_key_from_entry(entry, index):
        word = entry.word

        return MappingConnector._create_key(word, index)
