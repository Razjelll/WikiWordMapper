import itertools
from multiprocessing.pool import ThreadPool

from mappers.mapper import Mapper, MatrixFinder
from test import FileOpener


def start_test(mapper, wordnet, wiktionary, tests, wordnet_ids, values, finder):
    result_list = []
    for value in values:
        result = mapper._run_test(wordnet, wiktionary, tests, wordnet_ids, value, finder)
        result_list.append((result[0], result[1], value))
    return result_list


class WeightTester:

    @staticmethod
    def test(wordnet1_path, wordnet2_path, wiktionary_path, test_path):
        wiktionary, wordnet, wordnet_ids_map = WeightTester.__open_data(wiktionary_path, wordnet1_path, wordnet2_path)
        tests = FileOpener.read_test_file(test_path)
        values = itertools.combinations_with_replacement((0, 1, 2, 3, 4, 5), 8)
        used = set()

        best_values = None
        best_result = 0
        mapper = Mapper()
        finder = MatrixFinder(wordnet, wiktionary)
        pool = ThreadPool(processes=8)

        threads_results = []
        combinations = [value for value in values]
        print('Koniec')
        lists = WeightTester.even_divide(combinations, 8)
        for list in lists:
            threads_results.append(
                pool.apply_async(start_test, args=(mapper, wordnet, wiktionary, tests, wordnet_ids_map, list, finder)))
        pool.close()
        pool.join()
        results = [r.get() for r in threads_results]
        res = []
        for result in results:
            for value in result:
                correct = value[0]
                factors = value[2]
                if correct > best_result:
                    best_result = correct
                    best_values = factors

        print(best_result)
        print(best_values)

    @staticmethod
    def __open_data(wiktionary_path, wordnet1_path, wordnet2_path):
        wordnet = WordnetOpener.load_wordnet(wordnet1_path, wordnet2_path)
        wordnet_ids_map = WeightTester._create_wordnet_ids_map(wordnet)
        wiktionary = WiktionaryOpener.load_wiktionary(wiktionary_path)
        return wiktionary, wordnet, wordnet_ids_map

    @staticmethod
    def _create_wordnet_ids_map(wordnet):
        result_map = {}
        for word, units in wordnet.items():
            for unit in units:
                id = unit.id
                result_map[id] = unit
        return result_map

    @staticmethod
    def even_divide(lst, num_piece=4):
        return [
            [lst[i] for i in range(len(lst)) if (i % num_piece) == r]
            for r in range(num_piece)
        ]
