from mappers.score import Score
from values import RelationTypes, Language
from gensim.models.wrappers import FastText

class BestChain:

    def __init__(self, score_result, wiktionary_units, factors):
        self._score_result = score_result
        self._wiktionary_units = wiktionary_units
        self._factors = factors
        self._methods = []

    def append(self, method_name):
        self._methods.append(method_name)

    def start(self):
        for method_name in self._methods:
            best_unit, x = method_name(self._score_result, self._wiktionary_units, self._factors)
            if best_unit:
                return best_unit, x
        return None, []


# TODO: prawdopodobnie będzie można zrobić częśc współną z łańcuchów
class ScoreChain:

    def __init__(self, score_result, wiktionary_units, factors):
        self._score_result = score_result
        self._wiktionary_units = wiktionary_units
        self._factors = factors
        self._methods = []

    def append(self, method_name):
        self._methods.append(method_name)

    def start(self):
        for method_name in self._methods:
            scores = method_name(self._score_result, self._wiktionary_units, self._factors)
            if not any(scores):  # TODO: upewnić się, czyto na pewno tak ma być
                return scores
        return None  # TODO: tutaj może nalezy zwócić pustą listę


class MatrixFinder:
    DEFINITIONS_COLUMNS = [3, 4, 6]

    def __init__(self, wordnet=None, wiktionary=None, language=Language.PL):
        self._score_result = self._get_score_result(language, wordnet, wiktionary)
        self._wordnet = wordnet
        self._wiktionary = wiktionary

    def _get_score_result(self, language, wordnet, wiktionary):
        if language == Language.PL:
            return ScoreResult(wordnet, wiktionary, language)
        if language == Language.EN:
            return EnglishScoreResult(wordnet, wiktionary, language)

    # INFO: można pobawić się jeszcze innymi metodami : get_best_by_sum ...
    def find_best(self, wordnet_unit, wiktionary_units, factors):
        score_result = self._score_result.get_score(wordnet_unit, wiktionary_units)

        chain = BestChain(score_result, wiktionary_units, factors)
        chain.append(self._find_best_unit_by_commons_without_definition)
        chain.append(self._find_best_unit_by_all_commons)
        chain.append(self._find_best_unit)
        best_unit, x = chain.start()
        return best_unit, x

    def get_scores(self, wordnet_unit, wiktionary_units, factors):
        score_result = self._score_result.get_score(wordnet_unit, wiktionary_units)
        scores = self._get_scores(score_result, wiktionary_units, factors)

        return scores

    def _get_scores_by_all_without_definition(self, score_result, wiktionary_units, factors):
        score_result_without_definition = self._filter_score_result(score_result, MatrixFinder.DEFINITIONS_COLUMNS,
                                                                    False)
        scores = self._get_scores(score_result_without_definition, wiktionary_units, factors)
        return scores

    def _get_scores_by_common_without_definition(self, score_result, wiktionary_units, factors):
        common_features_without_definition = self._get_common_features_without_definition(score_result)
        score_result_without_definition = self._filter_score_result(score_result, common_features_without_definition,
                                                                    True)
        scores = self._get_scores(score_result_without_definition, wiktionary_units, factors)
        return scores

    def _get_scores_by_all_commons(self, score_result, wiktionary_units, factors):
        common_features = self._get_common_features(score_result)
        scores = self._get_scores(score_result, wiktionary_units, factors)
        return scores

    def _find_best_by_all_without_definition(self, score_result, wiktionary_units, factors):
        score_result_without_definition = self._filter_score_result(score_result, MatrixFinder.DEFINITIONS_COLUMNS,
                                                                    False)
        best_unit, x = self._find_best_unit(score_result_without_definition, wiktionary_units, factors)
        return best_unit, x  # TODO: we wszystkich metodach pozmieniać nazwę x

    def _find_best_by_only_definitions(self, score_result, wiktionary_units, factors):
        score_result_only_defnitions = self._filter_score_result(score_result, MatrixFinder.DEFINITIONS_COLUMNS, True)
        best_unit, x = self._find_best_unit(score_result_only_defnitions, wiktionary_units, factors)
        return best_unit, x

    def _find_best_unit_by_all_commons(self, score_result, wiktionary_units, factors):
        common_features = self._get_common_features(score_result)
        best_unit, x = self._find_best_unit(score_result, wiktionary_units, factors, common_features)
        return best_unit, x

    def _find_best_unit_by_commons_without_definition(self, score_result, wiktionary_units, factors):
        common_features_without_definition = self._get_common_features_without_definition(score_result)
        score_result_without_definition = self._filter_score_result(score_result, common_features_without_definition,
                                                                    True)
        best_unit, x = self._find_best_unit(score_result_without_definition, wiktionary_units, factors)
        return best_unit, x

    # TODO: refaktoryzacja
    def _filter_score_result(self, score_result, columns, retain=True):
        filtered_result = []
        for unit_index, features in enumerate(score_result):
            filtered_result.append([])
            for feature_index, feature_value in enumerate(features):
                if retain:
                    if feature_index in columns:
                        filtered_result[unit_index].append(feature_value)
                    else:
                        filtered_result[unit_index].append(None)
                else:
                    if feature_index in columns:
                        filtered_result[unit_index].append(None)
                    else:
                        filtered_result[unit_index].append(feature_value)
        return filtered_result

    def _get_common_features_without_definition(self, score_result):
        definition_columns = [3, 4, 6]
        common_features = self._get_common_features(score_result)
        non_definition_features = [value for value in common_features if value not in definition_columns]
        return non_definition_features

    def _get_common_features(self, score_result):
        features_counter = [0 for x in range(len(score_result[0]))]
        for unit_index, features in enumerate(score_result):
            for feature_index, feature_value in enumerate(features):
                if feature_value is not None:  # TODO: można dorzucić warunke != 0
                    features_counter[feature_index] += 1

        units_num = len(score_result)
        common_features = [index for index, value in enumerate(features_counter) if value == units_num]
        return common_features

    def _find_best_unit(self, score_result, wiktionary_units, factors, common_features=None):
        best_score = 0
        best_unit = None
        equal_units = []
        if not common_features:
            common_features = [index for index, x in enumerate(score_result[0])]
        for index, values in enumerate(score_result):
            score = sum(
                [x * factors[index] for index, x in enumerate(values) if index in common_features and x is not None])
            if score > best_score:
                best_score = score
                best_unit = wiktionary_units[index]
                equal_units.clear()
            if score == best_score:
                equal_units.append(wiktionary_units[index])
        return best_unit, equal_units
        # TODO: dla każdej cechy wybrać największa wartość

    def _get_scores(self, score_result, wiktionary_units, factors, common_features=None):
        if not common_features:
            common_features = [index for index, _ in enumerate(score_result[0])]
        result_map = {}
        for index, values in enumerate(score_result):
            score = sum(
                [x * factors[index] for index, x in enumerate(values) if index in common_features and x is not None])
            if score:
                unit = wiktionary_units[index]
                result_map[unit] = score
        return result_map

    def _get_max_features_values(self, score_result):
        max_values = [0 for x in score_result[0]]  # TODO: sprawdzić, czy to nie spowoduje błędu
        transposed_matrix = [list(i) for i in zip(*score_result)]
        for index, feature_values in enumerate(transposed_matrix):
            values = [x for x in feature_values if x is not None]
            if len(values) != 0:
                max_value = max(values)
                max_values[index] = max_value
            else:
                max_values[index] = 0
        return max_values

    def _get_best_weighted(self, score_result, wiktionary_units):
        # INFO: wynik 64
        best_score = 0
        best_unit = None
        counter_result = self._count_values(score_result)
        for index, values in enumerate(score_result):
            score = 0
            counter = counter_result[index]
            for value in values:
                if value:
                    score += value
            if counter:  # TODO: to nie
                score = score / counter
            else:
                continue
            if score > best_score:
                best_score = score
                best_unit = wiktionary_units[index]
        return best_unit, []

    def _get_best_by_sum_score(self, score_result, wiktionary_units):
        # INFO: wynik 63
        best_score = 0
        best_unit = None
        for index, values in enumerate(score_result):
            score = 0
            for value in values:
                if value:
                    score += value
            if score > best_score:
                best_score = score
                best_unit = wiktionary_units[index]
        return best_unit, []  # TODO: zastanwoić się, czy konflikty tutaj będą potrzebne

    # TODO: zmienić znazwę tego
    def _count_values(self, score_result):
        counter_result = [0 for x in score_result]
        for index, values in enumerate(score_result):
            counter = 0
            for value in values:
                if value is not None:
                    counter += 1
            counter_result[index] = counter
        return counter_result

    def _get_best_by_count_score(self, score_result, wiktionary_units):
        # INFO: wynik 61
        best_score = 0
        best_unit = None
        for index, values in enumerate(score_result):
            counter = 0
            for value in values:
                if value:
                    counter += 1
            if counter > best_score:
                best_score = counter
                best_unit = wiktionary_units[index]
        return best_unit, []


class ScoreResult:

    def __init__(self, wordnet=None, wiktionary=None, language=Language.PL):
        # path = '/media/roman/SeagateExpansion/Pobrane/Do pracy/crawl-300d-2M-subword.bin' # TODO: przenieść to gdzieś
        # embeddings = FastText.load_fasttext_format(path, encoding='ISO-8859-1')
        # self._common_score = Score(wordnet, wiktionary, language, embeddings)
        self._common_score = Score(wordnet, wiktionary, language)


    def get_score(self, wordnet_unit, wiktionary_units):
        # relations = self._get_relations_score(wordnet_unit, wiktionary_units)
        structure_relations = self._get_structure_relations_score(wordnet_unit, wiktionary_units)
        part_relations = self._get_part_relations_score(wordnet_unit, wiktionary_units)
        synonymy_relations = self._get_synonymy_relations_score(wordnet_unit, wiktionary_units)
        definitions = self._get_definitions_score(wordnet_unit, wiktionary_units)
        # definitions_hypernyms = self._get_definitions_hypernyms_score(wordnet_unit, wiktionary_units)
        definitions_hypernyms = [None for x in range(
            len(wiktionary_units))]  # TODO: wstawiono tutaj None, tak daje lepsze wyniki. Można się jeszcze tym pobawić
        translations = self._get_translations_score(wordnet_unit, wiktionary_units)
        hypernyms = self._get_hypernyms_in_definition_score(wordnet_unit, wiktionary_units)
        # hypernyms =  [None for x in range(len(wiktionary_units))]
        examples = self._get_examples_score(wordnet_unit, wiktionary_units)
        links = self._get_links_score(wordnet_unit, wiktionary_units)
        registers = self._get_registers_score(wordnet_unit, wiktionary_units)
        domains = self._get_domains_score(wordnet_unit, wiktionary_units)

        result = []
        for index, unit in enumerate(wiktionary_units):
            result.append([structure_relations[index], part_relations[index], synonymy_relations[index],
                           definitions[index], definitions_hypernyms[index], translations[index], hypernyms[index],
                           examples[index], links[index], registers[index], domains[index]])

        return result

    def get_score2(self, wordnet_unit, wiktionary_units):
        relations = self._get_relations_score(wordnet_unit, wiktionary_units)
        definitions = self._get_definitions_score(wordnet_unit, wiktionary_units)
        hypernyms = self._get_hypernyms_in_definition_score(wordnet_unit, wiktionary_units)
        translations = self._get_translations_score(wordnet_unit, wiktionary_units)
        examples = self._get_examples_score(wordnet_unit, wiktionary_units)

        result = []
        for index, unit in enumerate(wiktionary_units):
            result.append(
                (relations[index], definitions[index], hypernyms[index], translations[index], examples[index]))
        return result

    def _get_registers_score(self, wordnet_unit, wiktionary_units):
        scores = [self._common_score.score_registers(wordnet_unit, wiktionary_unit)
                  if wordnet_unit.register and wiktionary_unit.registers and len(
            wiktionary_unit.registers) != 0 else None
                  for wiktionary_unit in wiktionary_units]
        return scores

    def _get_domains_score(self, wordnet_unit, wiktionary_units):
        scores = [self._common_score.score_domains(wordnet_unit, wiktionary_unit)
                  if wordnet_unit.domain and wiktionary_unit.registers and len(wiktionary_unit.registers) != 0 else None
                  for wiktionary_unit in wiktionary_units]
        return scores

    def _get_structure_relations_score(self, wordnet_unit, wiktionary_units):
        scores = [self._common_score.score_structure_relations(wordnet_unit, wiktionary_unit)
                  if len(wordnet_unit.relations) != 0 and len(wiktionary_unit.relations) != 0 else None
                  for wiktionary_unit in wiktionary_units]
        return scores

    def _get_part_relations_score(self, wordnet_unit, wiktionary_units):
        scores = [self._common_score.score_part_relations(wordnet_unit, wiktionary_unit)
                  if len(wordnet_unit.relations) != 0 and len(wiktionary_unit.relations) != 0 else None
                  for wiktionary_unit in wiktionary_units]
        return scores

    def _get_synonymy_relations_score(self, wordnet_unit, wiktionary_units):
        scores = [self._common_score.score_synonymy_relations(wordnet_unit, wiktionary_unit)
                  if len(wordnet_unit.relations) != 0 and len(wiktionary_unit.relations) != 0 else None
                  for wiktionary_unit in wiktionary_units]
        return scores

    def _get_relations_score(self, wordnet_unit, wiktionary_units):
        scores = []
        wordnet_relations = set([type for type in wordnet_unit.relations.keys() if
                                 type not in [RelationTypes.HYPERNYM, RelationTypes.I_SYNONYM]])
        for wiktionary_unit in wiktionary_units:
            common_relations = wordnet_relations.intersection(wiktionary_unit.relations.keys())
            if len(common_relations):
                score = self._common_score.score_relations(wordnet_unit, wiktionary_unit)
                scores.append(score)
            else:
                scores.append(None)
        return scores

    def _get_definitions_score(self, wordnet_unit, wiktionary_units):
        scores = [self._common_score.score_definition(wordnet_unit, wiktionary_unit)
                  if self._are_definitions(wordnet_unit, wiktionary_unit) else None
                  for wiktionary_unit in wiktionary_units]
        return scores

    def _get_definition_by_embeddings_score(self, wordnet_unit, wiktionary_units):
        scores = [self._common_score.score_definition_by_embeddings(wordnet_unit, wiktionary_unit)
                 if self._are_definitions(wordnet_unit, wiktionary_unit) else None
                 for wiktionary_unit in wiktionary_units]
        return scores

    def _are_definitions(self, wordnet_unit, wiktionary_unit):
        return wordnet_unit.definition and wiktionary_unit.definition

    def _get_definitions_with_hypernyms_score(self, wordnet_unit, wiktionary_units):
        scores = [self._common_score.score_definition_with_hypernyms(wordnet_unit, wiktionary_unit)
                  if self._are_definitions(wordnet_unit, wiktionary_unit) else None
                  for wiktionary_unit in wiktionary_units]
        return scores

    def _get_definitions_with_hypernyms_synonyms_score(self, wordnet_unit, wiktionary_units):
        scores = [self._common_score.score_definition_with_hypernyms(wordnet_unit, wiktionary_unit)
                  if self._are_definitions(wordnet_unit, wiktionary_unit) else None
                  for wiktionary_unit in wiktionary_units]
        return scores

    def _get_definitions_with_synonyms_score(self, wordnet_unit, wiktionary_units):
        scores = [self._common_score.score_definition_with_synonyms(wordnet_unit, wiktionary_unit)
                  if self._are_definitions(wordnet_unit, wiktionary_unit) else None
                  for wiktionary_unit in wiktionary_units]
        return scores

    def _get_entire_definition_score(self, wordnet_unit, wiktionary_units):
        scores = [self._common_score.score_entire_definition(wordnet_unit, wiktionary_unit)
                  if self._are_definitions(wordnet_unit, wiktionary_unit) else None
                  for wiktionary_unit in wiktionary_units]
        return scores

    def _get_entire_definition_stem_score(self, wordnet_unit, wiktionary_units):
        scores = [self._common_score.score_entire_definition_stem(wordnet_unit, wiktionary_unit)
                  if self._are_definitions(wordnet_unit, wiktionary_unit) else None
                  for wiktionary_unit in wiktionary_units]
        return scores

    def _get_hypernyms_in_definition_score(self, wordnet_unit, wiktionary_units):
        scores = [self._common_score.score_hypernyms_in_definition(wordnet_unit, wiktionary_unit)
                  if (wordnet_unit.definition and RelationTypes.HYPERNYM in wiktionary_unit.relations)
                     or (RelationTypes.HYPERNYM in wordnet_unit.relations and wiktionary_unit.definition) else None
                  for wiktionary_unit in wiktionary_units]
        return scores

    def _get_hypernyms_with_synonyms_in_definition_score(self, wordnet_unit, wiktionary_units):
        scores = [self._common_score.score_hypernyms_with_synonyms_in_definition(wordnet_unit, wiktionary_unit)
                  if (wordnet_unit.definition and RelationTypes.HYPERNYM in wiktionary_unit.relations)
                     or (RelationTypes.HYPERNYM in wordnet_unit.relations and wiktionary_unit.definition) else None
                  for wiktionary_unit in wiktionary_units]
        return scores

    def _get_synonyms_in_definition_score(self, wordnet_unit, wiktionary_units):
        scores = [self._common_score.score_synonyms_in_definition(wordnet_unit, wiktionary_unit)
                  if RelationTypes.SYNONYM in wordnet_unit.relations and wiktionary_unit.definition else None
                  for wiktionary_unit in wiktionary_units]
        return scores

    def _get_part_relations_in_definition_score(self, wordnet_unit, wiktionary_units):
        scores = [self._common_score.score_part_relations_in_defintiion(wordnet_unit, wiktionary_unit)
                  if RelationTypes.SYNONYM in wordnet_unit.relations and wiktionary_unit.definition else None
                  for wiktionary_unit in wiktionary_units]
        return scores

    def _get_translations_score(self, wordnet_unit, wiktionary_units):
        scores = [self._common_score.score_translations(wordnet_unit, wiktionary_unit)
                  if RelationTypes.I_SYNONYM in wordnet_unit.relations and wiktionary_unit.translations and len(
            wiktionary_unit.translations) != 0 else None
                  for wiktionary_unit in wiktionary_units]
        return scores

    def _get_examples_score(self, wordnet_unit, wiktionary_units):
        scores = [self._common_score.score_examples(wordnet_unit, wiktionary_unit)
                  if len(wordnet_unit.examples) != 0 and len(wiktionary_unit.examples) != 0 else None
                  for wiktionary_unit in wiktionary_units]
        return scores

    def _get_links_score(self, wordnet_unit, wiktionary_units):
        scores = [self._common_score.score_links(wordnet_unit, wiktionary_unit)
                  if wordnet_unit.link and wiktionary_unit.link else None
                  for wiktionary_unit in wiktionary_units]
        return scores


class EnglishScoreResult(ScoreResult):

    def __init__(sel, wordnet=None, wiktionary=None, language=Language.EN):
        super().__init__(wordnet, wiktionary, language)

    def get_score(self, wordnet_unit, wiktionary_units):
        structure_relations = self._get_structure_relations_score(wordnet_unit, wiktionary_units)
        part_relations = self._get_part_relations_score(wordnet_unit, wiktionary_units)
        synonymy_relations = self._get_synonymy_relations_score(wordnet_unit, wiktionary_units)
        definitions = self._get_entire_definition_stem_score(wordnet_unit, wiktionary_units)
        # definitions_embeddings = self._get_definition_by_embeddings_score(wordnet_unit, wiktionary_units)
        # definitions = self._get_definition_score(wordnet_unit, wiktionary_units)
        # hypernyms_in_definition = self._get_hypernyms_in_definition_score(wordnet_unit, wiktionary_units)
        hypernyms_in_definition = self._get_hypernyms_with_synonyms_in_definition_score(wordnet_unit, wiktionary_units) # INFO: 2 możliwości do wyboryu
        synonyms_id_definition = self._get_synonyms_in_definition_score(wordnet_unit, wiktionary_units)
        part_relations_in_definition = self._get_part_relations_in_definition_score(wordnet_unit, wiktionary_units)
        definitions_with_hypernyms = self._get_definitions_with_hypernyms_score(wordnet_unit, wiktionary_units)
        definitions_with_synonyms = self._get_definitions_with_synonyms_score(wordnet_unit, wiktionary_units)
        # definitions = self._get_definitions_score(wordnet_unit, wiktionary_units)
        # TODO: prawdopodobnie będzie trzeba zrobić filtrowanie progu
        translations = self._get_translations_score(wordnet_unit, wiktionary_units)
        examples = self._get_examples_score(wordnet_unit, wiktionary_units)

        result = []
        for index, unit in enumerate(wiktionary_units):
            result.append([structure_relations[index], part_relations[index], synonymy_relations[index],
                           definitions[index], hypernyms_in_definition[index], synonyms_id_definition[index],
                           part_relations_in_definition[index],
                           # definitions_with_hypernyms[index],
                           # definitions_with_synonyms[index],
                           # definitions_embeddings[index],
                           translations[index], examples[index]])
        return result
