from values import RelationTypes


class MixFinder:

    def __init__(self, wordnet=None, wiktionary=None):
        self._common_score = Score(wordnet, wiktionary)

    def find_best(self, wordnet_unit, wiktionary_units, factors):
        existing_features = self._get_existing_features(wordnet_unit, wiktionary_units)
        best_wiktionary_unit, equal_units = self._find_best_unit(wordnet_unit, wiktionary_units, existing_features)
        # if best_wiktionary_unit is None: # TODO: odkomencić to
        #     features = [True for x in range(len(existing_features))]
        #     best_wiktionary_unit, equal_units = self._find_best_unit(wordnet_unit, wiktionary_units, features)

        return best_wiktionary_unit, equal_units

    def _find_best_unit(self, wordnet_unit, wiktionary_units, existing_features):
        best_wiktionary_unit = None
        best_score = 0
        equal_units = []
        for wiktionary_unit in wiktionary_units:
            score = self._score(wordnet_unit, wiktionary_unit, existing_features)
            if score > best_score:
                best_wiktionary_unit = wiktionary_unit
                best_score = score
                equal_units.clear()
            if score == best_score:
                equal_units.append(wiktionary_unit)
        # TODO: można zwrócić posortowaną listę wszystkich
        return best_wiktionary_unit, equal_units

    # TODO: sprawdzić, jak zachowają się definicje do pierwszego przecinka
    def _score(self, wordnet_unit, wiktionary_unit, existing_features: list):
        # TODO: przydają się tutaj stałe
        score = 0
        features = existing_features
        if not any(features):  # TODO: chyba
            features = [True for i in features]
        if features[0]:
            relations_score = self._common_score.score_relations(wordnet_unit.relations, wiktionary_unit.relations)
            score += relations_score
        if features[1] and wordnet_unit.definition and wiktionary_unit.definition:
            definition_score = self._common_score.score_definition(wordnet_unit, wiktionary_unit)
            score += definition_score
        if features[2]:
            hypernyms_score = self._common_score.score_hypernyms_in_definition(wordnet_unit, wiktionary_unit)
            score += hypernyms_score
        if features[3]:
            translations_score = self._common_score.score_translations(wordnet_unit, wiktionary_unit)
            score += translations_score
        if features[4]:
            examples_score = self._common_score.score_examples(wordnet_unit, wiktionary_unit)
            score += examples_score
        return score

    def _get_existing_features(self, wordnet_unit, wiktionary_units):
        RELATIONS = 0
        DEFINITIONS = 1
        HYPERNYMS = 2
        TRANSLATIONS = 3
        EXAMPLES = 4
        existing_features = [True, True, True, True, True]  # relations, definition, hypernyms, translations
        wordnet_relations = set([type for type in wordnet_unit.relations.keys()
                                 if type not in [RelationTypes.HYPERNYM, RelationTypes.I_SYNONYM]])

        if not wordnet_unit.definition:
            existing_features[DEFINITIONS] = False
        if not RelationTypes.HYPERNYM in wordnet_unit.relations:
            existing_features[HYPERNYMS] = False
        if not RelationTypes.I_SYNONYM in wordnet_unit.relations:
            existing_features[TRANSLATIONS] = False
        if len(wordnet_unit.examples) == 0:
            existing_features[EXAMPLES] = False

        for wiktionary_unit in wiktionary_units:
            common_relations = wordnet_relations.intersection(wiktionary_unit.relations.keys())
            if len(common_relations) == 0:
                existing_features[RELATIONS] = False
            if not wiktionary_unit.definition:
                existing_features[DEFINITIONS] = False
            if not RelationTypes.HYPERNYM in wiktionary_unit.relations:
                existing_features[HYPERNYMS] = False
            if not wiktionary_unit.translations or len(wiktionary_unit.translations) == 0:
                existing_features[TRANSLATIONS] = False
            if len(wiktionary_unit.examples) == 0:
                existing_features[EXAMPLES] = False

        return existing_features
