class OrderedFinder:
    # TODO: uzupełnić tę klasę 
    def find_best_unit(self, wordnet_unit, wiktionary_units):
        # TODO: można pomyśleć nad kolejnością
        best_unit = None
        if best_unit is None:
            best_unit = self._get_most_similar_unit_by_translations(wordnet_unit, wiktionary_units)
        if best_unit is None:
            best_unit = self._get_most_similar_unit_by_definition(wordnet_unit, wiktionary_units)
        if best_unit is None:
            best_unit = self._get_most_similar_unit_by_hiperonim(wordnet_unit, wiktionary_units)
        if best_unit is None:
            best_unit = self._get_most_similar_unit_by_relations(wordnet_unit, wiktionary_units)
        return best_unit, []

    def _get_most_similar_unit_by_translations(self, wordnet_unit, wiktionary_units):
        THRESHOLD = 0.3
        best_score = 0
        best_unit = None
        for wiktionary_unit in wiktionary_units:
            score = self._score_translations(wordnet_unit, wiktionary_unit)
            if score > THRESHOLD and score > best_score:
                best_score = score
                best_unit = wiktionary_unit
        return best_unit

    def _get_most_similar_unit_by_relations(self, wordnet_unit, wiktionary_units):
        THRESHOLD = 0.3
        wordnet_relations = wordnet_unit.relations
        best_score = 0
        best_unit = None
        for wiktionary_unit in wiktionary_units:
            wiktionary_relations = wiktionary_unit.relations
            score = self._score_relations(wordnet_unit, wiktionary_unit)
            # if len(wiktionary_relations) > 0:
            #     score = score / len(wiktionary_relations)
            if score > THRESHOLD and score > best_score:
                best_score = score
                best_unit = wiktionary_unit
        return best_unit

    def _get_most_similar_unit_by_hiperonim(self, wordnet_unit, wiktionary_units):
        THRESHOLD = 0.1
        best_score = 0
        best_unit = None
        for wiktionary_unit in wiktionary_units:
            score = self._score_hypernyms(wordnet_unit, wiktionary_unit)
            if score > THRESHOLD and score > best_score:
                best_score = score
                best_unit = wiktionary_unit
        return best_unit

    def _get_most_similar_unit_by_definition(self, wordnet_unit, wiktionary_units):
        if wordnet_unit.definition is None:
            return 0
        THRESHOLD = 0.1
        best_score = 0
        best_unit = None
        for wiktionary_unit in wiktionary_units:
            if wiktionary_unit.definition is None:
                continue
            score = self._score_hypernyms(wordnet_unit, wiktionary_unit)
            min_length = min(len(wordnet_unit.definition), len(wiktionary_unit.definition))
            if min_length > 0:
                score = score / min(len(wordnet_unit.definition), len(wiktionary_unit.definition))
                if score > THRESHOLD and score > best_score:
                    print('Osiągnięto próg definicja')
                    best_score = score
                    best_unit = wiktionary_unit
        return best_unit
