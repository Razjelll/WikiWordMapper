from nltk.stem import PorterStemmer

from utils.sentence_analyzer import PolishAnalyzer, EnglishAnalyzer, Analyzer
from values import RelationTypes, PartOfSpeech, REGISTERS, DOMAINS, Language
import numpy as np
from scipy.spatial.distance import cosine

class Score:

    def __init__(self, wordnet=None, wiktionary=None, language=Language.PL, embeddings = None):
        self._analyzer = self._get_analyzer(language)
        self._embeddings = embeddings
        self._wordnet = wordnet
        self._wiktionary = wiktionary

    def _get_analyzer(self, language):
        if language == Language.PL:
            return PolishAnalyzer()
        if language == Language.EN:
            return EnglishAnalyzer()

    def score_links(self, wordnet_unit, wiktionary_unit):
        return LinkScore.score(wordnet_unit, wiktionary_unit)

    def score_examples(self, wordnet_unit, wiktionary_unit):
        return ExampleScore.score(wordnet_unit, wiktionary_unit, self._analyzer)

    def score_translations(self, wordnet_unit, wiktionary_unit):
        return TranslationScore.score(wordnet_unit, wiktionary_unit)

    def score_relations(self, wordnet_unit, wiktionary_unit):
        return RelationScore.score(wordnet_unit, wiktionary_unit)

    def score_synonymy_relations(self, wordnet_unit, wiktionary_unit):
        return RelationScore.score_synonymy(wordnet_unit, wiktionary_unit)

    def score_part_relations(self, wordnet_unit, wiktionary_unit):
        return RelationScore.score_part(wordnet_unit, wiktionary_unit)

    def score_structure_relations(self, wordnet_unit, wiktionary_unit):
        return RelationScore.score_structure(wordnet_unit, wiktionary_unit)

    def score_definition(self, wordnet_unit, wiktionary_unit):
        return DefinitionScore.score(wordnet_unit, wiktionary_unit, self._analyzer)

    def score_definition_by_embeddings(self, wordnet_unit, wiktionary_unit):
        return DefinitionScore.score_embeddings(wordnet_unit, wiktionary_unit, self._analyzer, self._embeddings)

    def score_definition_with_hypernyms(self, wordnet_unit, wiktionary_unit):
        return DefinitionScore.score_with_hypernyms(wordnet_unit, wiktionary_unit, self._analyzer, self._wordnet)

    def score_definition_with_synonyms(self, wordnet_unit, wiktionary_unit):
        return DefinitionScore.score_with_synonyms(wordnet_unit, wiktionary_unit, self._analyzer, self._wordnet)

    def score_entire_definition(self, wordnet_unit, wiktionary_unit):
        return DefinitionScore.score_entire(wordnet_unit, wiktionary_unit, self._analyzer)



    def score_entire_definition_stem(self, wordnet_unit, wiktionary_unit):
        return DefinitionScore.score_entire_stem(wordnet_unit, wiktionary_unit, self._analyzer)

    def score_hypernyms_in_definition(self, wordnet_unit, wiktionary_unit):
        return DefinitionHypernymsScore.score(wordnet_unit, wiktionary_unit, self._analyzer)

    def score_hypernyms_with_synonyms_in_definition(self, wordnet_unit, wiktionary_unit):
        return DefinitionHypernymsScore.score_with_synonyms(wordnet_unit, wiktionary_unit, self._analyzer)

    def score_synonyms_in_definition(self, wordnet_unit, wiktionary_unit):
        return DefinitionSynonymsScore.score(wordnet_unit, wiktionary_unit, self._analyzer)

    def score_part_relations_in_defintiion(self, wordnet_unit, wiktionary_unit):
        return DefinitionPartRelationsScore.score(wordnet_unit, wiktionary_unit, self._analyzer)

    def score_registers(self, wordnet_unit, wiktionary_unit):
        return RegisterScore.score(wordnet_unit, wiktionary_unit)

    def score_domains(self, wordnet_unit, wiktionary_unit):
        return DomainScore.score(wordnet_unit, wiktionary_unit)


class ExampleScore:

    @staticmethod
    def score(wordnet_unit, wiktionary_unit, analyzer):
        wordnet_examples_elements = ExampleScore._get_examples_elements(wordnet_unit.examples, analyzer)
        wiktionary_examples_elements = ExampleScore._get_examples_elements(wiktionary_unit.examples, analyzer)
        common_elements = wordnet_examples_elements.intersection(wiktionary_examples_elements)
        score = ExampleScore._get_example_score(len(common_elements), wordnet_examples_elements,
                                                wiktionary_examples_elements)
        return score

    @staticmethod
    def _get_examples_elements(examples, analyzer):
        elements = set()
        for example in examples:
            example_elements = analyzer.analyse(example)
            words = [element[PolishAnalyzer.WORD] for element in example_elements]
            elements.update(words)
        return elements

    @staticmethod
    def _get_example_score(score, wordnet_elements, wiktionary_elements):
        length = len(wordnet_elements) + len(wiktionary_elements)
        return score * 2 / length


class TranslationScore:

    @staticmethod
    def score(wordnet_unit, wiktionary_unit):
        if RelationTypes.I_SYNONYM in wordnet_unit.relations and wiktionary_unit.translations:
            wordnet_translations = [related_unit.word for related_unit in
                                    wordnet_unit.relations[RelationTypes.I_SYNONYM]]
            common_translations = set(wordnet_translations).intersection(wiktionary_unit.translations)
            score = TranslationScore._get_translations_score(common_translations, wordnet_translations,
                                                             wiktionary_unit.translations)
            return score
        return 0

    @staticmethod
    def _get_translations_score(common_translations, wordnet_translations, wiktionary_translations):
        # INFO: podzielenie dało lepszy wynik o 1
        length = len(wordnet_translations) + len(wiktionary_translations)
        return len(common_translations) * 2 / length


class RelationScore:

    @staticmethod
    def score(wordnet_unit, wiktionary_unit):
        wordnet_unit_relations = wordnet_unit.relations
        wiktionary_unit_relations = wiktionary_unit.relations
        score = 0
        all_relations = 0
        if wordnet_unit_relations and wiktionary_unit_relations:
            for type, wordnet_relations in wordnet_unit_relations.items():
                # TODO: sprawdzić z liczeniem wszystkich relacji. Nie tylko tych, które są w obu jednostkach
                if type in wiktionary_unit_relations:
                    wiktionary_rel = wiktionary_unit_relations[type]
                    wordnet_rel = set([unit.word for unit in wordnet_relations])
                    common_elements = wordnet_rel.intersection(wiktionary_rel)
                    score += len(common_elements)
                    if type != RelationTypes.I_SYNONYM:
                        all_relations += len(wordnet_rel) + len(wiktionary_rel)
        return score

    @staticmethod
    def score_structure(wordnet_unit, wiktionary_unit):
        # TODO: tymczasowa forma metody. Należy przyjąć, że jeżeli wspólne zarówno hiperonimy jak i holonimy, to powinna być bardziej punktowana
        relation_types = [RelationTypes.HOLONYM, RelationTypes.MERONYM]
        score = RelationScore._score_relations(wordnet_unit.relations, wiktionary_unit.relations, relation_types)
        return score

    @staticmethod
    def score_part(wordnet_unit, wiktionary_unit):
        wordnet_unit_relations = wordnet_unit.relations
        wiktionary_unit_relations = wiktionary_unit.relations
        relation_types = [RelationTypes.HOLONYM, RelationTypes.MERONYM]
        score = RelationScore._score_relations(wordnet_unit_relations, wiktionary_unit_relations, relation_types)
        return score

    @staticmethod
    def score_synonymy(wordnet_unit, wiktionary_unit):
        wordnet_unit_relations = wordnet_unit.relations
        wiktionary_unit_relations = wiktionary_unit.relations
        relation_types = [RelationTypes.SYNONYM, RelationTypes.ANTONYM]
        score = RelationScore._score_relations(wordnet_unit_relations, wiktionary_unit_relations, relation_types)
        return score

    @staticmethod
    def _score_relations(wordnet_unit_relations, wiktionary_unit_relations, relation_types):
        score = 0
        wordnet_relations_counter = 0
        wiktionary_relations_counter = 0
        if wordnet_unit_relations and wiktionary_unit_relations:
            for type, wordnet_relations in wordnet_unit_relations.items():
                if type in relation_types and type in wiktionary_unit_relations:
                    wiktionary_rel = wiktionary_unit_relations[type]
                    wordnet_rel = set([unit.word for unit in wordnet_relations])
                    common_elements = wordnet_rel.intersection(wiktionary_rel)

                    score += len(common_elements)
                    wordnet_relations_counter += len(wordnet_rel)
                    wiktionary_relations_counter += len(wiktionary_rel)
        return RelationScore._get_relations_score(score, wordnet_relations_counter, wiktionary_relations_counter)

    @staticmethod
    def _get_relations_score(score, wordnet_relations, wiktionary_relations):
        if (wordnet_relations + wiktionary_relations) != 0:
            return score / (wordnet_relations + wiktionary_relations)
        return 0


class DefinitionScore:

    @staticmethod
    def score(wordnet_unit, wiktionary_unit, analyzer):
        poses = [PartOfSpeech.NOUN, PartOfSpeech.VERB]
        wordnet_definition = wordnet_unit.definition
        wiktionary_definition = wiktionary_unit.definition
        delimiter = ';' if ';' in wordnet_definition else ','
        wordnet_definition = wordnet_definition.split(delimiter)[0]
        score = DefinitionScore._score_definition(wordnet_definition, wiktionary_definition, analyzer, poses)
        return score

    @staticmethod
    def score_embeddings(wordnet_unit, wiktionary_unit, analyzer, embeddings):
        # poses = [PartOfSpeech.NOUN, PartOfSpeech.VERB, PartOfSpeech.ADJECTIVE, PartOfSpeech.ADVERB]
        poses = [PartOfSpeech.NOUN, PartOfSpeech.VERB, PartOfSpeech.ADJECTIVE]
        # wordnet_definition = wordnet_unit.definition.split(',')[0]
        # wiktionary_definition = wiktionary_unit.definition.split(',')[0]
        wordnet_definition = wordnet_unit.definition
        wiktionary_definition = wiktionary_unit.definition
        wordnet_definition_elements = analyzer.analyse(wordnet_definition)
        wiktionary_definition_elements = analyzer.analyse(wiktionary_definition)
        # filtrowanie
        wordnet_definition_elements = DefinitionScore._filter_elements_by_pos(wordnet_definition_elements, poses)
        wikitonary_definition_elements = DefinitionScore._filter_elements_by_pos(wiktionary_definition_elements, poses)

        wordnet_vectors = DefinitionScore._get_vectors(wordnet_definition_elements, embeddings)
        wiktionary_vectors = DefinitionScore._get_vectors(wiktionary_definition_elements, embeddings)

        wordnet_average_vector = np.average(wordnet_vectors, axis=0)
        wiktionary_average_vector = np.average(wiktionary_vectors, axis=0)

        score = cosine(wordnet_average_vector, wiktionary_average_vector)
        socre = 1 - score

        return score

    @staticmethod
    def _get_vectors(elements, embeddings):
        embeddings_list = []
        for element in elements:
            word = element[Analyzer.WORD]
            if word in embeddings:
                vector = embeddings.wv[word]
                embeddings_list.append(vector)
        return embeddings_list


    @staticmethod
    def score_entire_stem(wordnet_unit, wiktionary_unit, analyzer):
        # poses = [PartOfSpeech.NOUN, PartOfSpeech.VERB, PartOfSpeech.ADJECTIVE, PartOfSpeech.ADVERB]
        poses = [PartOfSpeech.NOUN, PartOfSpeech.VERB]
        score = DefinitionScore._score_definition_stem(wordnet_unit.definition, wiktionary_unit.definition, analyzer,
                                                       poses)
        return score

    @staticmethod
    def score_entire(wordnet_unit, wiktionary_unit, analyzer):
        poses = [PartOfSpeech.NOUN, PartOfSpeech.VERB]
        score = DefinitionScore._score_definition(wordnet_unit.definition, wiktionary_unit.definition, analyzer, poses)
        return score

    @staticmethod
    def _score_definition(wordnet_definition, wiktionary_definition, analyzer, poses):
        wordnet_definition_elements = analyzer.analyse(wordnet_definition)
        wiktionary_definition_elements = analyzer.analyse(wiktionary_definition)

        wordnet_definition_elements = DefinitionScore._filter_elements_by_pos(wordnet_definition_elements, poses)
        wiktionary_definition_elements = DefinitionScore._filter_elements_by_pos(wiktionary_definition_elements, poses)
        common_parts = set(wordnet_definition_elements).intersection(set(wiktionary_definition_elements))

        score = DefinitionScore._get_definition_score(common_parts, wordnet_definition_elements,
                                                      wiktionary_definition_elements)
        return score

    @staticmethod
    def _score_definition_stem(wordnet_definition, wiktionary_definition, analyzer, poses):
        # TODO zmienić nazwę, zrobić połączenie z podstawową metodą
        wordnet_definition_elements = analyzer.analyse(wordnet_definition)
        wiktionary_definition_elements = analyzer.analyse(wiktionary_definition)
        wordnet_definition_elements = DefinitionScore._filter_elements_by_pos(wordnet_definition_elements, poses)
        wiktionary_definition_elements = DefinitionScore._filter_elements_by_pos(wiktionary_definition_elements, poses)
        stemmer = PorterStemmer()  # TODO: przenieść to gdzieś, aby obiekt nie był tworzony tak często
        stemmed_wordnet_elements = DefinitionScore._get_stems(wordnet_definition_elements, stemmer)
        stemmed_wiktionary_elements = DefinitionScore._get_stems(wiktionary_definition_elements, stemmer)

        # common_parts = set(wordnet_definition_elements).intersection(set(wiktionary_definition_elements))
        common_parts = set(stemmed_wordnet_elements).intersection(set(stemmed_wiktionary_elements))

        score = DefinitionScore._get_definition_score(common_parts, wordnet_definition_elements,
                                                      wiktionary_definition_elements)
        return score

    @staticmethod
    def _get_stems(elements, stemer):
        result = []
        for element in elements:
            word = element[Analyzer.WORD]

            stemmed_word = stemer.stem(word)
            result.append(stemmed_word)
            # result.append(word)
        return result

    @staticmethod
    def _filter_elements_by_pos(elements, poses):
        return [element for element in elements if element[1] in poses]

    @staticmethod
    def _get_definition_score(common_elements, wordnet_elements, wiktionary_elements):
        if len(wordnet_elements) == 0 or len(wiktionary_elements) == 0:
            return 0  # TODO: albo zrobić None
        lenght = max(min(len(wordnet_elements), len(wiktionary_elements)), 1)
        # score = DefinitionScore._score_common_definitions_elements(common_elements)
        wordnet_score = DefinitionScore._score_common_definitions_elements(wordnet_elements)
        wiktionary_score = DefinitionScore._score_common_definitions_elements(wiktionary_elements)
        score = len(common_elements)
        wordnet_score = len(wordnet_elements)
        wiktionary_score = len(wiktionary_elements)
        score = score * 4 / (wordnet_score + wiktionary_score)  # INFO: * 4 daje lepsze wyniki
        return score

    @staticmethod
    def _score_common_definitions_elements(common_elements):
        # TODO: spróbować się tym pobawić
        score = 0
        for element in common_elements:
            pos = element[1]
            if pos == PartOfSpeech.NOUN:
                score += 3
            elif pos == PartOfSpeech.ADJECTIVE:
                score += 2
            elif pos == PartOfSpeech.VERB:
                score += 2
            elif pos == PartOfSpeech.ADVERB:
                score += 1
        return score

    # TODO: wyciągnąć część wspólną z synonimami
    @staticmethod
    def score_with_hypernyms(wordnet_unit, wiktionary_unit, analyzer, wordnet):
        wordnet_definition = wordnet_unit.definition
        wiktionary_definition = wiktionary_unit.definition
        # TODO: można zrobić podział. Na razie zrobię bez podziału
        # wordnet_definition = wordnet_definition.split(',')[0]
        # wiktionary_definition = wiktionary_definition.split(',')[0]
        wordnet_definition_elements = analyzer.analyse(wordnet_definition)
        wiktionary_definition_elements = analyzer.analyse(wiktionary_definition)
        # TODO: tutaj można zrobić filtrowanie. Na razie bez filtrowania
        POSES = [PartOfSpeech.NOUN, PartOfSpeech.VERB]
        wordnet_definition_elements = DefinitionScore._filter_elements_by_pos(wordnet_definition_elements, POSES)
        wiktionary_definition_elements = DefinitionScore._filter_elements_by_pos(wiktionary_definition_elements, POSES)
        wordnet_definition_words = [element[Analyzer.WORD] for element in wordnet_definition_elements]
        wiktionary_definition_words = [element[Analyzer.WORD] for element in wordnet_definition_elements]

        # TODO: refaktoryzacja. Sprawdzić, czy to daje jakieś efekty

        wordnet_parts = []
        wordnet_hypernyms = Hypernyms.get_hypernyms_list(wordnet_definition_elements, wordnet)
        wordnet_parts.extend(wordnet_definition_words)
        wordnet_parts.extend(wordnet_hypernyms)

        wiktionary_parts = []
        wiktionary_hypernyms = Hypernyms.get_hypernyms_list(wiktionary_definition_elements, wordnet)
        # wiktionary_parts.extend(wiktionary_definition_words)
        wiktionary_parts.extend(wiktionary_hypernyms)

        common_parts = set(wordnet_parts).intersection(set(wiktionary_parts))
        score = DefinitionScore._get_definition_with_hypernyms_score(common_parts, wordnet_parts, wiktionary_parts)

        return score

    @staticmethod
    def score_with_synonyms(wordnet_unit, wiktionary_unit, analyzer, wordnet):
        wordnet_definition = wordnet_unit.definition
        wiktionary_definition = wordnet_unit.definition
        # TODO: można zrobić ćwiartowanie definicji
        wordnet_definition_elements = analyzer.analyse(wordnet_definition)
        wiktionary_definition_elements = analyzer.analyse(wiktionary_definition)
        # TODO: tutaj można zrobić filtrowanie
        wordnet_definition_words = [element[Analyzer.WORD] for element in wordnet_definition_elements]
        wiktionary_definition_words = [element[Analyzer.WORD] for element in wordnet_definition_elements]

        wordnet_parts = []
        wordnet_synonyms = Synonyms.get_synonyms_list(wordnet_definition_elements, wordnet)
        # wordnet_parts.extend(wordnet_definition_words)
        wordnet_parts.extend(wordnet_synonyms)

        wiktionary_parts = []
        wiktionary_synonyms = Synonyms.get_synonyms_list(wiktionary_definition_elements, wordnet)
        wiktionary_parts.extend(wiktionary_definition_words)
        wiktionary_parts.extend(wiktionary_synonyms)

        common_parts = set(wordnet_parts).intersection(set(wiktionary_parts))

        score = DefinitionScore._get_definition_with_synonyms_score(common_parts, wordnet_parts, wiktionary_parts)
        return score

    @staticmethod
    def _get_definition_with_synonyms_score(common_parts, wordnet_parts, wiktionary_parts):
        common_size = len(common_parts)
        wordnet_size = len(wordnet_parts)
        wiktionary_size = len(wiktionary_parts)

        size = wordnet_size + wiktionary_size
        if size:
            score = common_size / (wordnet_size + wiktionary_size)
            return score
        return 0

    @staticmethod
    def _get_definition_with_hypernyms_score(common_parts, wordnet_parts, wiktionary_parts):
        common_size = len(common_parts)
        wordnet_size = len(wordnet_parts)
        wiktionary_size = len(wiktionary_parts)
        size = wordnet_size + wiktionary_size
        if size:
            return common_size / size
        return 0


class DefinitionHypernymsScore:

    @staticmethod
    def score(wordnet_unit, wiktionary_unit, analyzer):
        HYPERNYMS_LEVEL = 3
        hypernyms = Hypernyms.get_hypernyms(wordnet_unit, HYPERNYMS_LEVEL)
        hypernyms_words = [hypernym.word for hypernym in hypernyms]
        exists = DefinitionHypernymsScore._check_words_are_in_definition(hypernyms_words, wiktionary_unit.definition,
                                                                         analyzer)
        score = 1 if exists else 0
        return score

    @staticmethod
    def score_with_synonyms(wordnet_unit, wiktionary_unit, analyzer):
        HYPERNYMS_LEVEL = 3
        hypernyms = Hypernyms.get_hypernyms_with_synonyms(wordnet_unit, HYPERNYMS_LEVEL)
        hypernyms_words = [hypernym.word for hypernym in hypernyms]
        exists = DefinitionHypernymsScore._check_words_are_in_definition(hypernyms_words, wiktionary_unit.definition,
                                                                         analyzer)

        score = 1 if exists else 0
        return score

    @staticmethod
    def _check_words_are_in_definition(words, definition, analyzer):
        # TODO: może wprowadzić w zalezności od głębokości hiperonimu
        if not definition:
            return 0
        definition_parts = analyzer.get_base_words(definition)
        for word in words:
            if ' ' in word and word in definition:  # wielowyrazowiec
                return DefinitionHypernymsScore._check_multiword_in_definition(word, definition_parts, analyzer)
            if word in definition_parts:
                return True
        return False

    @staticmethod
    def _check_multiword_in_definition(multiword, definition_parts, analyzer):
        # TODO: sprawdzić jeszcze, czy to działa poprawnie
        elements = analyzer.analyse(multiword)
        for element in elements:
            word = element[PolishAnalyzer.WORD]
            pos = element[PolishAnalyzer.POS]
            if word not in definition_parts:
                return False
        return True


class DefinitionSynonymsScore:

    @staticmethod
    def score(wordnet_unit, wiktionary_unit, analyzer):
        synonyms = DefinitionSynonymsScore._get_synonyms_from_wordnet(wordnet_unit)
        wiktionary_definition_elements = analyzer.analyse(wiktionary_unit.definition)
        wiktionary_definition_words = [element[Analyzer.WORD] for element in wiktionary_definition_elements]
        common_parts = set(synonyms).intersection(set(wiktionary_definition_words))
        score = DefinitionSynonymsScore._get_definition_synonyms_score(common_parts, synonyms,
                                                                       wiktionary_definition_words)
        return score

    @staticmethod
    def _get_definition_synonyms_score(common_parts, synonyms, wiktionary_definition_words):
        if len(common_parts) > 0:
            return 1
        return 0

    @staticmethod
    def _get_synonyms_from_wordnet(unit):
        if RelationTypes.SYNONYM in unit.relations:
            synonyms = unit.relations[RelationTypes.SYNONYM]
            synonyms_word = [unit.word for unit in synonyms]
            return synonyms_word
        return []


class DefinitionPartRelationsScore:

    @staticmethod
    def score(wordnet_unit, wiktionary_unit, analyzer):
        part_relations = DefinitionPartRelationsScore._get_part_relations(wordnet_unit)
        wiktionary_definition_elements = analyzer.analyse(wiktionary_unit.definition)
        wiktionary_definition_words = [element[Analyzer.WORD] for element in wiktionary_definition_elements]
        common_parts = set(part_relations).intersection(set(wiktionary_definition_words))
        score = DefinitionPartRelationsScore._get_definition_synonyms_score(common_parts, part_relations,
                                                                            wiktionary_definition_words)
        return score

    @staticmethod
    def _get_definition_synonyms_score(common_parts, synonyms, wiktionary_definition_words):
        if len(common_parts) > 0:
            return 1
        return 0

    @staticmethod
    def _get_part_relations(unit):
        # TODO: sprawdzić, czy pobieranie wielu poziomów holonimów i meronimów zadziała
        related_result = []
        if RelationTypes.HOLONYM in unit.relations:
            holonyms = unit.relations[RelationTypes.HOLONYM]
            holonyms_words = [unit.word for unit in holonyms]
            related_result.extend(holonyms_words)
        if RelationTypes.MERONYM in unit.relations:
            meronyms = unit.relations[RelationTypes.MERONYM]
            meronyms_words = [unit.word for unit in meronyms]
            related_result.extend(meronyms_words)
        return related_result


class LinkScore:

    @staticmethod
    def score(wordnet_unit, wiktionary_unit):
        wordnet_link = wordnet_unit.link
        wordnet_link_parts = wordnet_link.split('/')
        wordnet_entry = wordnet_link_parts[len(wordnet_link_parts) - 1]
        wiktionary_entry = wiktionary_unit.link
        score = 1 if wordnet_entry == wiktionary_entry else 0
        return score


class RegisterScore:

    @staticmethod
    def score(wordnet_unit, wiktionary_unit):
        if RegisterScore._are_registers_exists(wordnet_unit, wiktionary_unit):
            wordnet_register = REGISTERS[wordnet_unit.register]
            return 1 if RegisterScore._check_registers_are_equal(wordnet_register, wiktionary_unit.registers) else 0
        return None

    @staticmethod
    def _are_registers_exists(wordnet_unit, wiktionary_unit):
        # TODO: możliwe, ze tutaj trzeba zrobić pustą listę
        return wordnet_unit.register and wordnet_unit.register in REGISTERS and wiktionary_unit.registers is not None

    @staticmethod
    def _check_registers_are_equal(wordnet_register, wiktionary_registers):
        for wiktionary_register in wiktionary_registers:
            if wordnet_register == wiktionary_register:  # TODO: sprawdzićm, czy równość jest ok
                return True
        return False


class DomainScore:

    @staticmethod
    def score(wordnet_unit, wiktionary_unit):
        if DomainScore._are_domains_exists(wordnet_unit, wiktionary_unit):
            wordnet_domain = DOMAINS[wordnet_unit.domain]
            return 1 if DomainScore._check_domains_are_equal(wordnet_domain, wiktionary_unit.registers) else 0
        return None

    @staticmethod
    def _are_domains_exists(wordnet_unit, wiktionary_unit):
        return wordnet_unit.domain and wordnet_unit.domain in DOMAINS and wiktionary_unit.registers is not None

    @staticmethod
    def _check_domains_are_equal(wordnet_domain, witkionary_registers):
        for wiktionary_register in witkionary_registers:
            if wordnet_domain in wiktionary_register or wiktionary_register in wordnet_domain:
                return True
        return False


class Hypernyms:

    # @staticmethod
    # def get_hypernyms_with_synonyms(elements, wordnet):
    #     result = []
    #     for element in elements:
    #         word = element[Analyzer.WORD]
    #         pos = element[Analyzer.POS]
    #         if pos == PartOfSpeech.NOUN:
    #             hypernyms = Hypernyms._find_hypernyms_with_synonyms_from_wordnet(word, wordnet)
    #             result.extend(hypernyms)
    #     return result

    @staticmethod
    def get_hypernyms_list(elements, wordnet):
        result = []
        for element in elements:
            word = element[Analyzer.WORD]
            pos = element[Analyzer.POS]
            if pos == PartOfSpeech.NOUN:
                hypernyms = Hypernyms._find_hypernyms_from_wordnet(word, wordnet)
                result.extend(hypernyms)
        return result

    @staticmethod
    def _find_hypernyms_from_wordnet(word, wordnet):
        HYPERNYMS_NUMBER = 2
        hypernyms_result = []
        if word in wordnet:
            units = wordnet[word]
            for unit in units:
                hypernyms = Hypernyms.get_hypernyms(unit, HYPERNYMS_NUMBER)
                hypernyms_words = [unit.word for unit in hypernyms]
                hypernyms_result.extend(hypernyms_words)
        return hypernyms_result

    @staticmethod
    def _find_hypernyms_with_synonyms_from_wordnet(word, wordnet):
        HYPERNYMS_NUMBER = 2  # TODO: koniecznie przerzucić to do parametru
        hypernyms_result = []
        if word in wordnet:
            units = wordnet[word]
            for unit in units:
                all_hypernyms_units = []
                hypernyms = Hypernyms.get_hypernyms(unit, HYPERNYMS_NUMBER)
                all_hypernyms_units.extend(hypernyms)
                for hypernym in hypernyms:
                    synonyms = Synonyms.get_synonyms_units(hypernym)
                    all_hypernyms_units.extend(synonyms)
                hypernyms_words = [unit.word for unit in hypernyms]
                hypernyms_result.extend(hypernyms_words)
        return hypernyms_result

    @staticmethod
    def get_hypernyms(wordnet_unit, hypernyms_number):
        stack = []
        stack.append((wordnet_unit, hypernyms_number))
        hypernyms_result = []
        used_units = set()
        while len(stack) > 0:
            current_entry = stack.pop()
            current_unit = current_entry[0]
            current_number = current_entry[1]
            if RelationTypes.HYPERNYM in current_unit.relations:
                for hypernym in current_unit.relations[RelationTypes.HYPERNYM]:
                    if current_number > 0 and hypernym not in used_units:
                        stack.append((hypernym, current_number - 1))
                        hypernyms_result.append(hypernym)
                        used_units.add(hypernym)
        return hypernyms_result

    @staticmethod
    def get_hypernyms_with_synonyms(wordnet_unit, hypernyms_number):
        stack = []
        stack.append((wordnet_unit, hypernyms_number))
        hypernyms_result = []
        used_units = set()
        while len(stack) > 0:
            current_entry = stack.pop()
            current_unit = current_entry[0]
            current_number = current_entry[1]
            if RelationTypes.HYPERNYM in current_unit.relations:
                for hypernym in current_unit.relations[RelationTypes.HYPERNYM]:
                    if current_number > 0 and hypernym not in used_units:
                        stack.append((hypernym, current_number - 1))
                        hypernyms_result.append(hypernym)
                        hypernyms_synonyms = Synonyms.get_synonyms_units(hypernym)
                        hypernyms_result.extend(hypernyms_synonyms)
                        used_units.add(hypernym)
        return hypernyms_result


class Synonyms:

    @staticmethod
    def get_synonyms_units(unit):
        if RelationTypes.SYNONYM in unit.relations:
            return unit.relations[RelationTypes.SYNONYM]
        return []

    @staticmethod
    def get_synonyms_list(elements, wordnet):
        result = []
        for element in elements:
            word = element[Analyzer.WORD]
            synonyms = Synonyms._find_synonyms_from_wordnet(word, wordnet)
            result.extend(synonyms)
        return result

    @staticmethod
    def _find_synonyms_from_wordnet(word, wordnet):
        synonyms_result = []
        if word in wordnet:
            units = wordnet[word]
            for unit in units:
                if RelationTypes.SYNONYM in unit.relations:
                    synonyms = unit.relations[RelationTypes.SYNONYM]
                    synonyms_words = [unit.word for unit in synonyms]
                    synonyms_result.extend(synonyms_words)
        return synonyms_result
