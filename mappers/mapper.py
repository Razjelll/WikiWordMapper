import json
import pickle

from loaders.wiktionary import WiktionaryOpener
from loaders.wordnet import WordnetOpener
from mappers.extra.inter_mapping import MappingLoader, MappingConnector
from mappers.finders.matrix import MatrixFinder
from mappers.json_saver import JsonSaver
from test import FileOpener
from values import PartOfSpeech, Language

NOUN_TAGS = ['subst', 'depr']
ADJECTIVE_TAGS = ['adj', 'adja', 'adjp']
VERB_TAGS = ['fin', 'bedzie', 'aglt', 'praet', 'impt', 'imps', 'inf', 'pcon', 'pant', 'ger', 'pact', 'ppas']
ADVERBS_TAG = ['adv']


# INFO: hiperonimy w definicji nie są oceniane, ponieważ dochodzi sporo negatywnych
class Mapper:

    def __init__(self, language=Language.PL):
        self._language = language

    def run(self, wordnet1_path, wordnet2_path, wiktionary_path):
        map_result = {}
        factors = [1] * 11
        wiktionary, wordnet, wordnet_ids_map = self.__open_data(wiktionary_path, wordnet1_path, wordnet2_path)
        finder = self._get_finder(wordnet, wiktionary)
        used_unit = set()
        for word, wordnet_units in wordnet.items():
            if word in wiktionary:
                wiktionary_units = wiktionary[word]
                mapping_result = self._find_bests(wordnet_units, wiktionary_units, finder, factors)
                map_result.update(mapping_result)
                # for wordnet_unit in wordnet_units:
                #     # TODO: filtrowanie rzeczowników
                #     # best_wiktionary_unit, _ = finder._find_bests(wordnet_unit, wiktionary_units, factors)
                #     mapping_result = self._find_bests(wordnet_units, wiktionary_units, finder, factors)
                #     if mapping_result:
                #         map_result[wordnet_unit] = mapping_result
            # return map_result
        print(len(map_result))
        return map_result

    # TODO: przy właściwym rzutowaniu
    def test(self, wordnet1_path, wordnet2_path, wiktionary_path, test_path):
        factors = [1] * 11  # TODO: ustawić jakąś stałą
        wiktionary, wordnet, wordnet_ids_map = self.__open_data(wiktionary_path, wordnet1_path, wordnet2_path)
        tests = FileOpener.read_test_file(test_path)
        return self._run_test(wordnet, wiktionary, tests, wordnet_ids_map, factors)

    def test2(self, wordnet1_path, wordnet2_path, wiktionary_path, test_path, language=Language.PL):
        factors = [1] * 11
        wiktionary, wordnet, wordnet_ids_map = self.__open_data(wiktionary_path, wordnet1_path, wordnet2_path)
        tests = FileOpener.read_test_file(test_path)
        return self._run_test2(wordnet, wiktionary, tests, wordnet_ids_map, factors)

    # TODO: ocenić wszystkie jednostki
    # TODO: wybrać najlepszą
    def _run_test(self, wordnet, wiktionary, tests, wordnet_ids_map, factors=None, finder=None):
        finder = self._get_finder(wordnet, wiktionary) if not finder else finder
        incorrects_list = []
        corrects_list = []
        for unit_id, wiktionary_entry in tests.items():
            correct_wiktionary_unit = self._get_wiktionary_unit(wiktionary, wiktionary_entry)
            wordnet_unit = self._get_wordnet_unit(unit_id, wordnet_ids_map)
            if wordnet_unit.word not in wiktionary:
                continue
            wiktionary_units = wiktionary[wordnet_unit.word]

            best_wiktionary_unit, equal_units = finder.find_best(wordnet_unit, wiktionary_units, factors)

            if best_wiktionary_unit == correct_wiktionary_unit:
                corrects_list.append(best_wiktionary_unit)
            elif best_wiktionary_unit:
                incorrects_list.append((best_wiktionary_unit, correct_wiktionary_unit, wordnet_unit, equal_units))
        correct_size = len(corrects_list)
        incorrect_size = len(incorrects_list)
        print(str(correct_size) + ' : ' + str(incorrect_size) + ' ' + str(factors))
        return correct_size, incorrect_size, corrects_list

    def _run_test2(self, wordnet, wiktionary, tests, wordnet_ids_map, factors=None, finder=None):
        self.__temp_wiktionary_processing(wiktionary)

        finder = self._get_finder(wordnet, wiktionary) if not finder else finder
        corrects_list = []
        incorrects_list = []
        not_mapped_list = []
        for unit_id, wiktionary_entry in tests.items():
            # TODO: pobrać słowo. Dla wszystkich jednostek z tym słowem zrobić
            wordnet_unit = self._get_wordnet_unit(unit_id, wordnet_ids_map)
            if wordnet_unit.word not in wordnet or wordnet_unit.word not in wiktionary:
                continue
            wordnet_units = wordnet[wordnet_unit.word]
            wiktionary_units = wiktionary[wordnet_unit.word]
            correct_wiktionary_unit = self._get_wiktionary_unit(wiktionary, wiktionary_entry)

            if wordnet_unit.word in ['incitement', 'timetable', 'seeker', 'dogfight']:
                print()

            mapping_result = self._find_bests(wordnet_units, wiktionary_units, finder, factors)
            if wordnet_unit in mapping_result:
                mapped_wiktionary_unit = mapping_result[wordnet_unit]
                if mapped_wiktionary_unit == correct_wiktionary_unit:
                    corrects_list.append(mapped_wiktionary_unit)
                else:
                    incorrects_list.append((mapped_wiktionary_unit, correct_wiktionary_unit, wordnet_unit))
            else:
                not_mapped_list.append((wordnet_unit, wiktionary_units, correct_wiktionary_unit))

        correct_size = len(corrects_list)
        incorrect_size = len(incorrects_list)
        print(str(correct_size) + ' : ' + str(incorrect_size) + ' ' + str(factors))
        return correct_size, incorrect_size, corrects_list

    def __temp_wiktionary_processing(self, wiktionary):
        path = '/media/roman/SeagateExpansion/Projects/Wiktionary/output/pl_en_mapping.json'
        result = MappingLoader.load(path)
        MappingConnector.connect(wiktionary, result)
        # TODO: rozwiązanie tymczasowe. Trzeba lepiej zapisać dane testowe, oraz zrobić ograniczenie, żeby szukało wśród rzeczowników (albo takiej samej części mowy jak jednostka wordnetu)
        for word, units in wiktionary.items():
            filtered_units = [unit for unit in units if unit.part_of_speech == PartOfSpeech.NOUN]
            wiktionary[word] = filtered_units

    def _find_bests(self, wordnet_units, wiktionary_units, finder, factors):
        score_results = []
        for wordnet_unit in wordnet_units:
            unit_scores = finder.get_scores(wordnet_unit, wiktionary_units, factors)
            for wiktionary_unit, score in unit_scores.items():
                score_results.append((wordnet_unit, wiktionary_unit, score))
        score_results.sort(key=lambda tup: -tup[2])
        used_wordnet_units = set()
        used_wiktionary_units = set()
        result = {}

        for elements in score_results:
            wordnet_unit = elements[0]
            wiktionary_unit = elements[1]
            score = elements[2]
            if wordnet_unit not in used_wordnet_units and wiktionary_unit not in used_wiktionary_units:
                result[wordnet_unit] = wiktionary_unit
                used_wordnet_units.add(wordnet_unit)
                used_wiktionary_units.add(wiktionary_unit)
        return result

    def __open_data(self, wiktionary_path, wordnet1_path, wordnet2_path):
        wordnet = WordnetOpener.load_wordnet(wordnet1_path, wordnet2_path)
        wordnet_ids_map = self._create_wordnet_ids_map(wordnet)
        wiktionary = WiktionaryOpener.load_wiktionary(wiktionary_path)
        return wiktionary, wordnet, wordnet_ids_map

    def _get_finder(self, wordnet, wiktionary):
        # finder = MixFinder(wordnet, wiktionary)
        finder = MatrixFinder(wordnet, wiktionary, self._language)
        # score = ScoreResult()
        # finder = OrderedFinder(wordnet_wiktionary)
        return finder

    def _get_wiktionary_unit(self, wiktionary, wiktionary_entry):
        wiktionary_word = wiktionary_entry[0]
        meaning_number = int(wiktionary_entry[1])
        if wiktionary_word not in wiktionary:
            print(wiktionary_word)
            return None
        wiktionary_units = wiktionary[wiktionary_word]
        wiktionary_unit = wiktionary_units[meaning_number - 1]
        return wiktionary_unit

    def _get_wordnet_unit(self, unit_id, wordnet_ids_map):
        unit_id = int(unit_id)
        wordnet_unit = wordnet_ids_map[unit_id]
        return wordnet_unit

    def _create_wordnet_ids_map(self, wordnet):
        result_map = {}
        for word, units in wordnet.items():
            for unit in units:
                id = unit.id
                result_map[id] = unit
        return result_map

class Saver:

    @staticmethod
    def save(mapping_result, path):
        result_map = {}
        for wordnet_unit, wiktionary_entry in mapping_result.items():
            wordnet_id = wordnet_unit.id
            # entry = JsonSaver._create_meaning_map(wiktionary_entry)
            # result_map[str(wordnet_id)] = entry
            entry_dictionary = JsonSaver.get_entry_to_dict(wiktionary_entry)
            result_map[wordnet_id] = entry_dictionary
        # pickle.dump(result_map, open('output/map_result.pck', 'wb'))
        with open(path, 'w') as file:
            file.write(json.dumps(result_map))
