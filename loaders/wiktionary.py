# coding=utf-8

import json

from values import RelationTypes

DEFINITION = 'definition'
PART_OF_SPEECH = 'part_of_speech'
GENDER = 'gender'
EXAMPLES = 'examples'
REGISTERS = 'registers'
RELATIONS = 'relations'
TRANSLATIONS = 'translations'
LINK = 'link'

class WiktionaryOpener:


    @staticmethod
    def load_wiktionary(path):
        result = {}
        with open(path, 'r') as file:
            wiktionary = json.load(file)

        for word, meanings in wiktionary.items():

            for index, meaning in enumerate(meanings):
                entry = WiktionaryOpener._load_entry(word, index, meaning)
                if word not in result:
                    result[word] = []
                result[word].append(entry)
        return result

    @staticmethod
    def _load_entry(word, index, meaning):
        entry = Entry()
        entry.word = word
        entry.number = index + 1
        entry.definition = meaning[DEFINITION]
        if PART_OF_SPEECH in meaning:
            entry.part_of_speech = meaning[PART_OF_SPEECH]
        if GENDER in meaning:
            entry.gender = meaning[GENDER]
        if REGISTERS in meaning:
            entry.registers = meaning[REGISTERS]
        if EXAMPLES in meaning:
            entry.examples = meaning[EXAMPLES]
        if TRANSLATIONS in meaning:
            entry.translations = WiktionaryOpener._get_translations(meaning[TRANSLATIONS])
        if RELATIONS in meaning:
            relations = WiktionaryOpener._get_relations(meaning[RELATIONS])
            for key, values in relations.items():
                filtered_values = [x for x in values if x != '']
                relations[key] = filtered_values
            entry.relations = relations
        if LINK in meaning:
            entry.link = meaning[LINK]
        return entry

    @staticmethod
    def _get_translations(translations):
        # TODO: zastanowić się jak to powinno być. Do rzutownaia potrzebny jest tylko jeden język
        if 'en' in translations:
            translations_result = [element.replace(',','') for element in translations['en']]
            return translations_result
            # return translations['en']
        if 'pl' in translations:
            translations_result = [element.replace(',','') for element in translations['pl']]
            return translations_result

    @staticmethod
    def _get_relations(relations):
        # TODO: napisać to krótszym sposobem
        result = {}
        for type, relations_list in relations.items():
            relation_type = WiktionaryOpener._get_relation_type(type)
            result[relation_type] = relations_list
            # result[relation_type] = [element.replace(',',' ') for element in relations_list]
        return result

    @staticmethod
    def _get_relation_type(relation_type_text):
        map  = {'synonyms': RelationTypes.SYNONYM,
                'antonyms': RelationTypes.ANTONYM,
                'hypernyms': RelationTypes.HYPERNYM,
                'hyponyms': RelationTypes.HYPONYM,
                'meronyms': RelationTypes.MERONYM,
                'holonyms':RelationTypes.HOLONYM}
        if relation_type_text in map:
            return map[relation_type_text]

class Entry:

    def __init__(self):
        self.word = ''
        self.number = -1
        self.part_of_speech = -1
        self.definition = ''
        self.gender = ''
        self.registers = []
        self.examples = []
        self.translations = [] # only english translations
        self.relations = {}
        self.link = None

    def __str__(self):
        return '{} - {}'.format(self.word, self.definition)
