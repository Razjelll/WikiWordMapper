# coding=utf-8

import json

from values import RelationTypes

ID = 'id'
OFFSET = 'offset'
RELATIONS = 'relations'
WORD = 'word'
VARIANT = 'variant'
PART_OF_SPEECH = 'pos'
DOMAIN = 'domain'
LEXICON = 'lexicon'
DEFINTIION = 'definition'
LINK = 'link'
REGISTER = 'register'
ASPECT = 'aspect'
PROPER_NAME = 'proper_name'
UNITS = 'units'
SYNSETS = 'synsets'
EXAMPLES = 'examples'


HYPONYMS_IDS = [10, 173, 174]
# HYPERNYMS_IDS = [11, 171, 172]
HYPERNYMS_IDS = [11]
MERONYMS_IDS = [14, 178, 179, 180]
HOLONYMS_IDS = [15, 175, 176, 177]
ANTONYMS_IDS = [12, 170, 101, 104]
I_SYNONYMS_IDS = [208, 209, 3005, 3006]
# TODO: można zrobić jeszcze derywacje

class WordnetOpener:

    @staticmethod
    def load_wordnet(main_wordnet_path, secondary_wordnet_path):

        units_result = {}
        synsets_map = {}
        units_ids = {}

        with open(main_wordnet_path, 'r') as file:
            main_wordnet = json.load(file)

        with open(secondary_wordnet_path, 'r') as file:
            secondary_wordnet = json.load(file)
        # TODO: zmienić nazwę
        WordnetOpener._create_wordnet(main_wordnet, units_ids, synsets_map, units_result)
        WordnetOpener._create_wordnet(secondary_wordnet, units_ids, synsets_map)

        for synset in main_wordnet:
            if RELATIONS in synset:
                synset_relations = synset[RELATIONS]
                WordnetOpener._insert_synsets_relations(synset_relations, synsets_map)
            units = synset[UNITS]
            for unit in units:
                if RELATIONS in unit:
                    unit_relations = unit[RELATIONS]
                    WordnetOpener._insert_units_relations(unit_relations, units_ids)
        WordnetOpener._insert_synonyms_to_units(synsets_map)

        return units_result

    @staticmethod
    def _get_relation_type(relation_type_id):
        if relation_type_id in HYPERNYMS_IDS:
            return RelationTypes.HYPERNYM
        if relation_type_id in HYPONYMS_IDS:
            return RelationTypes.HYPONYM
        if relation_type_id in MERONYMS_IDS:
            return RelationTypes.MERONYM
        if relation_type_id in HOLONYMS_IDS:
            return RelationTypes.HOLONYM
        if relation_type_id in ANTONYMS_IDS:
            return RelationTypes.ANTONYM
        if relation_type_id in I_SYNONYMS_IDS:
            return RelationTypes.I_SYNONYM


    @staticmethod
    def _create_wordnet(wordnet, units_ids, synsets_map, units_result=None):
        for synset in wordnet:
            synset_id = synset[ID]
            units_values = synset[UNITS]
            units = WordnetOpener._get_units(units_values)
            synsets_map[synset_id] = units
            WordnetOpener._insert_units(units, units_ids, units_result)

    @staticmethod
    def _insert_synsets_relations(synset_relations, synset_map):
        for type, synsets_list in synset_relations.items():
            type = WordnetOpener._get_relation_type(int(type))
            if not type:
                continue # relation type is not supported
            # TODO: pobrać typ relacji
            for synsets in synsets_list:
                # TODO: zrobic pobieranie odpowiedniej relacji
                source_synset_id = synsets['source_id']
                target_synset_id = synsets['target_id']

                if source_synset_id in synset_map and target_synset_id in synset_map:
                    source_units = synset_map[source_synset_id]
                    target_units = synset_map[target_synset_id]

                    for source_unit in source_units:
                        source_unit.relations[type] = target_units
                        # TODO: sprawdzić to
        # TODO: wstawić tutaj tłumaczenia

    @staticmethod
    def _insert_units_relations(units_relations, units_ids):
        for type, units_list in units_relations.items():
            type = WordnetOpener._get_relation_type(int(type))
            if not type:
                continue
            for units in units_list:
                source_unit_id = units['source_id']
                target_unit_id = units['target_id']

                if source_unit_id in units_ids and target_unit_id in units_ids:
                    source_unit = units_ids[source_unit_id]
                    target_unit = units_ids[target_unit_id]

                    if type not in source_unit.relations:
                        source_unit.relations[type] = []
                    source_unit.relations[type].append(target_unit)

    @staticmethod
    def _insert_synonyms_to_units(synsets_map):
        for synset_id, units in synsets_map.items():
            for unit in units:
                synonyms_unit = [unit2 for unit2 in units if unit2 != unit]
                if RelationTypes.SYNONYM not in unit.relations:
                    unit.relations[RelationTypes.SYNONYM] = []
                unit.relations[RelationTypes.SYNONYM].extend(synonyms_unit)

    @staticmethod
    def _insert_units(units, units_ids, units_result=None):
        for unit in units:
            units_ids[unit.id] = unit
            if units_result is not None:
                word = unit.word
                if word not in units_result:
                    units_result[word] = []
                units_result[word].append(unit)

    @staticmethod
    def _get_units(units_values):
        # TODO: zlikwidować to if, taka sytuacja nie powinna mieć miejsca
        units_result = [WordnetOpener._get_unit(unit_value) for unit_value in units_values if unit_value[ID] is not None]
        return units_result

    @staticmethod
    def _get_unit(unit_values):
        unit = Unit()
        unit.id = int(unit_values[ID])
        unit.word = unit_values[WORD]
        unit.variant = int(unit_values[VARIANT])
        unit.part_of_speech = unit_values[PART_OF_SPEECH]
        if DOMAIN in unit_values:
            unit.domain = unit_values[DOMAIN]
        if LEXICON in unit_values:
            unit.lexicon = unit_values[LEXICON]
        if DEFINTIION in unit_values:
            unit.definition = unit_values[DEFINTIION]
        if REGISTER in unit_values:
            unit.register = unit_values[REGISTER]
        if ASPECT in unit_values:
            unit.aspect = unit_values[ASPECT]
        if EXAMPLES in unit_values:
            unit.examples = unit_values[EXAMPLES]
        if LINK in unit_values:
            unit.link = unit_values[LINK]

        unit.proper_name = PROPER_NAME in unit_values

        return unit


class Unit:
    def __init__(self):
        self.id = None
        self.word = None
        self.variant = None
        self.part_of_speech = None
        self.domain = None
        self.synset = None

        self.lexicon = None

        self.definition = None
        self.link = None
        self.register = None
        self.aspect = None
        self.proper_name = False
        self.relations = {}
        self.examples = []

    def __str__(self):
        return '{} {} - {}'.format(self.word, self.variant, self.definition)


class Synset:

    def __init__(self, id=None):
        self.id = id
        self.relations = []

# TODO: zastanowić się, w jakiej formie mają być relacje
